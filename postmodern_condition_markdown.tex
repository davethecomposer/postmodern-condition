\begin{epigraphs}
\qitem{\emph{The art or science of combining vocal or instrumental sounds to
produce beauty}}
{---\textsc{OED}}
\end{epigraphs}

\noindent There is a difference between a fundamental change in art\footnote{This
  essay applies equally to all art in all media. But I am a composer and
  so all my arguments and examples are taken from music. I am confident,
  though, that suitable examples and parallels can be found within all
  media. And I am making no claim that music did any of these things
  first.} and a stylistic one. There were three fundamental changes to
how we think about art that occurred during the 20\textsuperscript{th}
century. These defined the Modern and then the Postmodern Age.

A \emph{Fundamental} change works at the level of how we define art, how
we conceptualize it, how we relate to it as artists and audience. A
\emph{Stylistic} change works at a more superficial level. It is the
difference between Baroque music of Bach and the Classical period music
of Mozart. Between the early pop-music of the Beatles to the later
progressive style of their last few albums.

These style changes were revolutionary and took audiences by surprise.
They challenged the existing rules of what was considered ``good''
music, what was considered ``proper''. These artists and musicians
rebelled against previous generations coming up with ideas that took art
and music in whole new directions much to the consternation of their
elders\footnote{Those darn kids!}. Art, music, etc, would never be the
same. And in each generation the same conflicts and generation of the
new played itself out all over again.

But they did not challenge the very definition of music, of art. For
that we look elsewhen: the early years of the 20\textsuperscript{th}
century.

Schoenberg: ``if it is art, it is not for all, and if it is for all, it
is not art'' and ``my music is not lovely''. This was a fundamental
change in art. Artists had always created art that people didn't like or
was for the wrong audience. But rarely, unless their desire was to
shock, was it done on purpose. For the artists of the Modernist period
no longer was a positive popular reaction sought. It was not the goal.
In fact in some circles it was considered failure. But it wasn't
necessarily to shock but because art had changed.

In all previous recorded instances artists tried to create works that
would appeal to some kind of popular sentiment. It might be the populace
of the cultured and wealthy or of the uncultured and poor or even of the
ecclesiastical and ascetic but regardless of the size or significance it
tried to appeal to the tastes of an audience born and trained in a
culture's artistic conventions.

But these Modern artists decided that no longer must they even try to
appeal to any popular desires no matter how esoteric. This was a
fundamental change: Art that was ``ugly''.

Not incompetently made, not some new style that so departed from the
norm that conservatives and traditionalists balked, but art that was
ugly for art's sake and which thus became beautiful to the initiated.
Even if that was an initiate class of one.

In music we see this beginning with the atonal music of the 1890s and
formalized with Schoenberg's 12 Tone Method of 1921. 2500 years of music
evolving from the mystical tunings and scales of Ancient Greece, through
the Middle Ages, into the Renaissance and the development of harmony,
and up through the 1800s, was all rejected and tossed aside as so much
detritus. A new æsthetic had announced itself and fundamentally it
differed from everything before.

It differed because it no longer cared if it appealed to any audience.

This also marked the culmination of the Modern. Not the beginning, not
the end, but the point.

\section{Philosophical Digression}\label{philosophical-digression}

Interestingly there's an important philosophical implication that
probably would have been rejected by most modernists. That is was
impossible to rationally ground any æsthetic theory which claimed that
one piece of art was better than any other. In other words, all art was
now of equal quality.

As I said, I think just about every Modernist would have rejected this
notion had it even come up. As much as they spurned the conventional
\emph{theories} of the previous generations they were still caught up in
the glamour of being a Great Artist. In order to have Great Art/Artists
you must have non-Great Art/Artists.

But if these artists reject everything that had made art beautiful for
thousands of years and then also asserted that their art was beautiful
then there is a problem. And the only way out is to reject that there is
a non-arbitrary way in which to assert or prove that any piece of art is
objectively beautiful.

This is not to deny that we can meaningfully\footnote{If the word
  ``meaningfully'' stretches your skeptical credulity then perhaps
  ``usefully'' will work better?} talk about the objective properties of
a work, like the number of notes it has, what those notes are, the
harmonic movement of said notes, but that things like ``good'' and
``beautiful'' and ``bad'' and ``ugly'' are not such things which
\emph{inhere} the piece of art under consideration.

If these latter properties do not inhere---are a fundamental part of the
thing itself---works of art, then art cannot be said to be
\emph{objectively} ``good'' etc.

\section{PrePostModern}\label{prepostmodern}
\begin{epigraphs}
\qitem{\emph{The finite number of tones selected for musical use from the
infinity available in nature is organized into specific tone systems
through defined rational processes}}
{---\textsc{Die Musik in Geschichte und
Gegenwart}}
\end{epigraphs}

\noindent As the 20\textsuperscript{th} century continued along some people came
around to appreciating this new, Modern æsthetic. There will always be
something appealing in the new, maybe for rebellious youth, maybe the
older experienced crowd tired of the same thing over and over, whatever
the impetus one great lesson of the century is that there will always be
an audience for anything even that which ostensibly cares not if it even
has an audience.

In the late '40s and early '50s more changes were happening. The Modern
period, which was to linger a bit longer, began to give way to the
Postmodern during which transition two more fundamental assumptions
about how art was defined were to be challenged.

In 1951 Morton Feldman published his work \emph{Extensions 1} in which
the performance of the work was left to chance\footnote{In a nutshell
  the performers are told to perform any note they want within the
  prescribed ranges of ``high'', ``medium'', or ``low''.}. Not to
improvisation, which still conformed to \emph{some} kind of pre-existing
theoretical system, but something that, in the Modernist tradition,
rejected any conventional theory while challenging the assumption that
art is a product of the conscious decision making of the creator of the
work.

By making chance operations the defining element of this music Feldman
put to rest the absolute control that composers had over their music.
John Cage astutely observed that Feldman's piece was
\emph{indeterminate} with respect to performance. Cage realized that a
work could also be indeterminate with respect to composition which
resulted in his \emph{Music of Changes} where every single performance
indication was determined entirely by chance but the performance itself
was entirely determinate. Now not only was the performance outside the
composer's control but so was the compositional process.

Allowing art to be generated entirely via indeterminate processes
clearly challenged a fundamental assumption concerning the relationship
between the artist as creator and the art that resulted.

\section{Another Philosophical
Digression}\label{another-philosophical-digression}

Even if you disagreed with my previous digression that implicit in the
Modernist period is the notion that there is no objectively ``good'' or
``bad'' art, surely by now you agree that such a position is implicit
when art is allowed to be created entirely through chance operations.

Again, I'm not aware of any of the composers from this period explicitly
making this claim but if we entertain the notion that Cage believed that
his \emph{Music of Changes} was the aesthetic equal of a Beethoven's
\emph{Sonata Pathétique} or any of Chopin's \emph{Nocturnes} then we
must conclude that there is no way to also hold the view that ``beauty''
and ``ugliness'' were properties that can inhere art.

\section{Postmodernism}\label{postmodernism}
\begin{epigraphs}
\qitem{\emph{humanly organized sound}}
{---\textsc{\emph{How Musical is Man} by John
Blacking}}
\end{epigraphs}

\noindent We can now imagine the concerned art lover coming to terms with the
innovations of 20\textsuperscript{th} century art up to this point. Art
that was ugly and art that was generated entirely by random processes
outside the control of the composer and/or performer. But at the very
least it could be said that these indeterminate works were still the
product of a system set up by the artist. That the artist, as an
agnostic creator God, a demiurge, created the universe only to let it
evolve on its own without any more interference.

In other words, no matter how far removed, at least the artist was the
origin of art.

Well that sounds like another fundamental assumption that needs
challenging.

And with that we introduce John Cage's \emph{4'33''}.

In this famous work first performed in 1952, the instructions for the
performer(s) were quite simple. Be as quiet as possible. In other words
you can have your instrument with you but do not consciously make any
sounds with it or, for that matter, make any sounds at all.

Some people\footnote{See for example Kostelanetz.} assert that the music
comprised all the other sounds that could then be heard. That the sounds
of the environment was the music of Cage's piece.

Now we have a music which does not try to be beautiful, is not composed
through the conscious decision making of the composer (and in fact Cage
used chance processes to determine the lengths of each of the piece's
three movements), and does not even include any sounds intentionally
produced by a human performer or composer.

Music no longer required any creative human effort whatsoever. In fact
as the years went on and Cage refined the piece this became more clear
as Cage would encourage people to just listen. Wherever you are whatever
sounds are occurring, just listen and accept them and that is music.

And there we have it. We've hit an extreme point here. But where exactly
are we? What is the definition of music, of art, given Cage's
\emph{4'33''?}

\begin{epigraphs}
\qitem{\emph{Art is that toward which we have an æsthetic
experience.}\\{\emph{Music is that toward which we have an æsthetic
experience while paying attention aurally.}}}
{---\textsc{the author}}
\end{epigraphs}

Notice that we are no longer defining art, or music (or art in any
medium), in terms of the qualities that the art object possesses, but
the experience we, the subject, have when interacting with the object.
If you have the kind of experience with the item in question that you do
when regarding other objects that we conventionally agree is art then
you have experienced art.

The power of this definition is that it accounts for every work of art
that has been produced to date. It includes the non-beautiful music of
the first part of the 20\textsuperscript{th} century, the music produced
through chance operations, and even those works which were not created
through the intention of an artist or composer.

\section{Modern or Postmodern?}\label{modern-or-postmodern}

Determining when culture passes from one age to another is rarely a
simple matter free from debate. This one is no different. I don't want
to spend too much time on this but what I will say is that while I can
accept that Cage's \emph{Music of Changes} and Feldman's
\emph{Extensions 1} were modernist, I believe that \emph{4'33''} is best
seen as postmodern.

I base this analysis on a deconstruction of \emph{4'33''} \footnote{I'm
  using \emph{deconstruction} in the academic sense ala Derrida and not
  in the Food Network sense, say, where a ``deconstructed'' apple pie is where the parts of the pie (crust, apples, cinnamon, etc) are presented separately instead of as a delicious whole.}. When something is deconstructed we
expose the binaries that are assumed and observe how the text challenges
those binaries. \emph{4'33''} looks at the binary separating music from
non-music, music from noise and deconstructs this binary by saying that
there is no longer a difference between music and noise.

This would make \emph{4'33''} an example of a poststructuralist work
which today we equate with being postmodern.

\section{PostPostModern}\label{postpostmodern}

The next question, naturally, is, what's the next assumption we can
challenge? How much further can we stretch the definition of art, or
music? Or, and now we're getting to the point of this essay, are there
any assumptions left to challenge?

I've spent a long time thinking about this and cannot imagine that there
is any assumption left to be challenged. We have a definition that is
very close to saying that everything is art but doesn't quite make that
leap. It stops just short by allowing for anything to be
\emph{considered} art if someone has an aesthetic experience while
regarding the thing.

And if we did want to make the claim that even those things which aren't
being regarded as art (via an aesthetic experience) are themselves now
also art have we actually gotten anywhere? I'd say not. I crafted the
definition above not because it was explicitly generated by the products
of the 20\textsuperscript{th} century but because it accommodated all
works of art without having to state that everything is art and thus
preserving the utility of being able to make some kind of distinction
between art and non-art even if any such distinction is contingent
entirely upon the state of mind of the observer.

\section{The Postmodern Condition}\label{the-postmodern-condition}

What does this mean, then, for the contemporary Postmodern artist
wanting to follow in the steps of the avant-garde who got us to this
point? It means that there is nothing that is fundamentally new to do.
There are no fundamental assumptions about art left to challenge. The
avant-garde is dead, not because tastes have changed, but because there
is nowhere left to advance toward.

This does not mean there is nothing new to do. In fact there are
infinitely many works in infinitely many styles (remember those?) yet to be created. There
is no danger of running out of new art. The point of this essay is more
focused. Are there any fundamental assumptions about art left to
challenge? The answer is no.

Any work of art, any piece of music that will ever be created from this
point forward, will be subsumed by an existing aesthetic theoretical
framework. What this means is that no matter how crazy a new piece might
be it can still be explained by the general definition of art ``that
toward which we have an aesthetic experience''. No piece of art will
ever be able to extend that definition any further. And that is the
Postmodern condition.

\section{Postlude}\label{postlude}

And finally, the big question, what if I'm wrong about my whole thesis
that there is nothing fundamentally new to do in the arts? During the
fifteen years between when I first came to the conclusion reached in
this essay and now when I'm actually writing it all down, I've wondered
if there might not be another philosophical approach to the issue.

I had held out hope that Continental Philosophy might have something.
But once I came to the conclusion that \emph{4'33''} was a
poststructuralist piece then I became fairly convinced that a solution
was not going to be found there.

So where? I honestly have no idea. If I had an idea then I'd have a
counter to my argument. And I'd also have a new direction in life. Satie
once said ``Show me something new; I'll begin all over again''. I'm not
sure if I could do that especially if someone else figures it out before
me. If I don't come up with something but someone else does then that
will mean I have failed at being the pioneering artist/composer I set
out to be when I was a young composer. I really don't want that kind of
failure hanging over me.

But I'd rather know for sure that I'm wrong than be wrong and never find
out.

I think.

\section{A Final Philosophical
Digression}\label{a-final-philosophical-digression}

You still might not agree with my previous assertions that implicit in
Modernist æsthetics is the idea that there is no objectively grounded
method to determine if a work of art is ``good'' or ``bad'' but that's
OK, I'm off that wagon and on a new one. Where that attitude was
\emph{implicit} in Modernism I'm now claiming that it is
\emph{explicitly} stated by Postmodernists.

One definition of the Postmodern \emph{style} of art has the artist
embracing all styles of art with equanimity. That all that is left is
pastiche. Everything from ``low'' art to ``high'' art is regarded as
being on the same level. Bach ain't no better than Katy Perry.

I'm not saying that anyone has to agree with the implicitly or
explicitly stated opinions of Modernism or Postmodernism, but I do think
those observations follow from the artwork produced by those folks.

\section{PostPostlude}\label{postpostlude}

Let's clean up a few loose ends. Note, the following doesn't have much
to do with the preceding and should be considered supplemental at best.

Some folk, like Professor of Philosophy Julian Dodd, assert things like
\emph{4'33''} is philosophy but not music. Or art but not music. They
then provide reasons for this that all start with some definition of
art/music based on some æsthetic philosophy system they are defending.
They start with a definition for art and then judge works against this
definition with \emph{4'33''} failing to make the cut.

I would refer to this as a \emph{prescriptivist} approach to philosophy.
The approach I'm advocating would, contrariwise, be called a
\emph{descriptivist} philosophy. Instead of creating a system and then
judging works against that system, I create a system based on my
observation of how people think through these issues, though in this
case I tend to accept the most liberal views out there.

What I find interesting in the case of \emph{4'33''} is how the piece is
treated by classical music professionals. The piece has been performed
many, many times on stage (ex sum). In fact I wouldn't be surprised if
it gets performed at least once a month somewhere in the world.

It has also been recorded many times and is available for purchase in
multiple formats.

And you can buy the sheet music from the respected publisher Edition
Peters.

So my point is that it is treated like any other piece of music by
people who live and breath music (or at least a significant number of
them) therefore any definition of music must take it into account. Any
definition that excludes it feels to me like an instance of the tail
wagging the dog.

And while on the subject of \emph{4'33''} \footnote{Maybe that should
  have been the subtitle of this essay?}, I want to explore a curious
moment in its history. In 1951 Robert Rauschenberg, an artist friend of
Cage's, unleashed upon the world his white paintings (paintings all of
one color: white). Cage has credited this with giving him the
``courage'' to compose \emph{4'33''} or, in some versions, a kick in the
pants out of fear that music was falling behind the arts.

And art historian Julia Robinson has even insinuated that Cage's
generosity toward Rauschenberg here was more self-serving than anything.

What makes this interesting to me is that I'm not so sure that
Rauschenberg's white paintings really are analogous to \emph{4'33''.} A
better analogue, it seems to me, would have been a blank canvas. By
painting even just one color on the canvas the viewer is invited to
inspect that color and notice the strokes and the shade and other
technical aspects that the artist employed. With \emph{4'33''} the work
is better separated from the composer. In fact a better analogue to the
white paintings would be a piece of music comprising one note. As far as
I can tell no such work has been published though I would be very
surprised if someone during that time period (especially from Fluxus)
didn't compose/perform such a work.

In the end it's a trivial point and I don't want to take anything away
from what Rauschenberg did which was revolutionary and an important
moment in this entire story about challenging assumptions about art. In
fact if I understood the history of painting and the visual arts better
than I do then this essay might even spend more time on Rauschenberg's
contribution. But I do believe there could probably be an interesting
paper or two done looking at this moment and the relationship between
these two artists and these works.

One last musing on \emph{4'33''}. It is often claimed (see critic
Kostelanetz, for eg) that the sounds that occur during a performance of
the piece (the ambient sounds) constitute the music of the work. So the
tittering of an audience member and a police siren outside the concert
hall are the music.

I've always felt that this reading missed some important ideas from
Cage. Take this line from his \emph{45 Minutes for a Speaker} (1954).

\begin{quote}
A fugue is a more complicated game, but it can be broken up by a single
sound, say, from a fire engine.
\end{quote}

I've always found this line really interesting. When applied to
\emph{4'33''} you're left with the idea that a performance of
\emph{4'33''} cannot be ``broken up'' by the sound from a fire engine.
That, in accordance with Cage's interests in Zen Buddhism, it is OK that
these other sounds occur. That they are perfectly acceptable and that
nothing has been ruined.

Therefore the claim is \emph{not} that the fire engine is part of Cage's
piece (which also seems an odd claim, as if Cage were claiming
ownership, somehow, of the entire universe of sounds) but that it is a
sound that is always present and should not be seen as interfering with
the integrity of the piece.

Or take what Cage said about Rauschenberg's white paintings in his
article \emph{On Robert Rauschenberg, Artist, and his Work} (1961):

\begin{quote}
The white paintings were airports for the lights, shadows, and
particles.
\end{quote}

which seems to suggest something similar to what I said above in that
the painting is not composed of the shadows and lights but allows the
viewer to notice those shadows and lights in a way that might be
considered distracting when looking at most paintings.

I think this probably gets us closer to \emph{4'33''} than what
Kostelanetz and others would have.
